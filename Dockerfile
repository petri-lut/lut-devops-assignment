# specify a suitable source image
FROM node:18-bookworm

RUN mkdir /app

WORKDIR /app

COPY package*.json ./

RUN npm ci

# copy the application source code files
COPY app.js ./
COPY index.js ./

EXPOSE 3000

# specify the command which runs the application
CMD ["npm", "start"]
